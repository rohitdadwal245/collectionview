//
//  AppDelegate.h
//  collection view
//
//  Created by Clicklabs14 on 12/10/15.
//  Copyright (c) 2015 click lab. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

