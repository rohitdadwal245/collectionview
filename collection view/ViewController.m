//
//  ViewController.m
//  collection view
//
//  Created by Clicklabs14 on 12/10/15.
//  Copyright (c) 2015 click lab. All rights reserved.
//

#import "ViewController.h"
#import "CustomCell.h"
@interface ViewController ()
{
    NSArray*arrayOfImages;
    NSArray*arrayOfDescriptions;
    
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [[self mycollectionView]setDataSource:self];
    [[self mycollectionView]setDelegate:self];
    
    arrayOfImages=[[NSArray alloc]initWithObjects:@"1.jpg",@"2.jpg",@"3.jpg",@"4.jpg",@"6.jpg", nil];
    arrayOfDescriptions=[[NSArray alloc]initWithObjects:@"mark zuckerberg.jpg",@"two",@"three",@"four",@"five", nil];
    
}
//data source and delegate method
- (NSInteger)numberOfSectionsInCollectionView:(UICollectionView *)collectionView;
{
    return 1;
}
- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section;
{
    return [arrayOfDescriptions count];
}
- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier=@"cell";
    CustomCell*cell =[collectionView dequeueReusableCellWithReuseIdentifier:cellIdentifier forIndexPath:indexPath];
    [[cell myImage]setImage:[UIImage imageNamed:[arrayOfImages objectAtIndex:indexPath.item]]];
    [[cell myDescriptionLabel]setText:[arrayOfDescriptions objectAtIndex:indexPath.item]];
    return cell;
    
}




- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
